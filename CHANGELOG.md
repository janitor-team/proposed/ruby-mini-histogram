## Master

## 0.1.3

- Handle edge cases (https://github.com/zombocom/mini_histogram/pull/2)

## 0.1.2

- Add `edge` as alias to `edges`

## 0.1.1

- Fix multi histogram weights, with set_average_edges! method (https://github.com/zombocom/mini_histogram/pull/1)

## 0.1.0

- First
